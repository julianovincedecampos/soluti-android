package br.com.julianocampos.soluti.api;

import java.util.List;

import br.com.julianocampos.soluti.mvp.model.Certificado;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Juliano Vince on 21/08/2017.
 */

public interface CertificadoService {
    @GET("certificado/listar")
    Observable<List<Certificado>> getCertificados();
}
