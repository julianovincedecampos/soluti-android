package br.com.julianocampos.soluti.dependency_injection.module;

import br.com.julianocampos.soluti.dependency_injection.scope.CustomScope;
import br.com.julianocampos.soluti.mvp.MainScreenContract;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
@Module
public class MainModule {
    private MainScreenContract.View mView;

    public MainModule(MainScreenContract.View view){
        mView = view;
    }

    @Provides
    @CustomScope
    MainScreenContract.View providerMainScreenContractView(){
        return mView;
    }
}
