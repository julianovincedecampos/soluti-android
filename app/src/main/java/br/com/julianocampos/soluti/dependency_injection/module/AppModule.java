package br.com.julianocampos.soluti.dependency_injection.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application){
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application providerAplication(){
        return mApplication;
    }
}
