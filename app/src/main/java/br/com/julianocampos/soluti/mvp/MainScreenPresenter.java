package br.com.julianocampos.soluti.mvp;

import java.util.List;

import javax.inject.Inject;

import br.com.julianocampos.soluti.api.CertificadoService;
import br.com.julianocampos.soluti.mvp.model.Certificado;
import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
public class MainScreenPresenter implements MainScreenContract.Presenter {
    Retrofit mRetrofit;
    MainScreenContract.View mView;

    @Inject
    MainScreenPresenter(Retrofit retrofit,MainScreenContract.View view){
        mRetrofit =retrofit;
        mView = view;
    }


    @Override
    public void loadCertificados() {
        mRetrofit.create(CertificadoService.class).getCertificados().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Certificado>>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Certificado> certificados) {
                        mView.showCertificados(certificados);
                    }
                });
    }
}
