package br.com.julianocampos.soluti.mvp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.julianocampos.soluti.R;
import br.com.julianocampos.soluti.dependency_injection.App;
import br.com.julianocampos.soluti.dependency_injection.component.DaggerMainComponent;
import br.com.julianocampos.soluti.dependency_injection.module.MainModule;
import br.com.julianocampos.soluti.mvp.model.Certificado;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
public class MainActivity extends AppCompatActivity implements MainScreenContract.View{
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;

    @Inject
    MainScreenPresenter mainScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listViewCertificados);
        list = new ArrayList<>();

        DaggerMainComponent.builder()
                .netComponent(((App) getApplicationContext()).getNetComponent())
                .mainModule(new MainModule(this))
                .build().inject(this);

        mainScreenPresenter.loadCertificados();
    }

    @Override
    public void showCertificados(List<Certificado> certificados) {
        for (int i = 0; i < certificados.size(); i++) {
            list.add(certificados.get(i).getNome());
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    @Override
    public void showError(String message) {
        Toast.makeText (getApplicationContext (), "Error" + message, Toast.LENGTH_SHORT) .show ();
    }

    @Override
    public void showComplete() {
        Toast.makeText (getApplicationContext (), "Complete", Toast.LENGTH_SHORT) .show ();
    }


}
