package br.com.julianocampos.soluti.dependency_injection.component;

import javax.inject.Singleton;

import br.com.julianocampos.soluti.dependency_injection.module.AppModule;
import br.com.julianocampos.soluti.dependency_injection.module.NetModule;
import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    Retrofit retrofit();
}
