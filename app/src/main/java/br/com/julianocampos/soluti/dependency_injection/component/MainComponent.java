package br.com.julianocampos.soluti.dependency_injection.component;

import br.com.julianocampos.soluti.dependency_injection.module.MainModule;
import br.com.julianocampos.soluti.dependency_injection.scope.CustomScope;
import br.com.julianocampos.soluti.mvp.MainActivity;
import dagger.Component;

/**
 * Created by Juliano Vince on 21/08/2017.
 */
@CustomScope
@Component(dependencies = NetComponent.class, modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
