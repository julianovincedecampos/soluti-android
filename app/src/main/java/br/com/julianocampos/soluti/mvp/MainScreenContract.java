package br.com.julianocampos.soluti.mvp;

import java.util.List;

import br.com.julianocampos.soluti.mvp.model.Certificado;

/**
 * Created by Juliano Vince on 21/08/2017.
 */

public class MainScreenContract {
    public interface View {
       public void showCertificados(List<Certificado> certificados);

       public void showError(String message);

       public void showComplete();
    }

    public  interface Presenter {
        public void loadCertificados();
    }
}
