package br.com.julianocampos.soluti.dependency_injection;

import android.app.Application;

import br.com.julianocampos.soluti.dependency_injection.component.DaggerNetComponent;
import br.com.julianocampos.soluti.dependency_injection.component.NetComponent;
import br.com.julianocampos.soluti.dependency_injection.module.AppModule;
import br.com.julianocampos.soluti.dependency_injection.module.NetModule;

/**
 * Created by Juliano Vince on 21/08/2017.
 */

public class App extends Application {
    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mNetComponent = DaggerNetComponent
                        .builder()
                        .appModule(new AppModule(this))
                        .netModule(new NetModule("http://10.0.2.2:8080/soluti-ws/services/"))
                        .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
